/**
 * Created by michal on 12.12.15.
 */
module.exports = function (grunt) {
    /*require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);*/

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {   },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'src/main/webapp/scripts/event/event.html',
                        'src/main/webapp/**/*.html',
                        'src/main/webapp/scripts/**/*.js',
                        'src/main/webapp/scripts/**/*.html'

                    ]
                }
            },
            options: {
                watchTask: true,
                proxy: "notification:8080",
                port: "5000",
                reloadDelay: 2000,
                logLevel: "debug",
                ghostMode: false

            }


        }


    });

    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // define default task
    grunt.registerTask('start', ['browserSync','watch' ]);
};