package com.sunday.domain.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;


public class SecurityUtils {


    public static Collection<? extends GrantedAuthority> getUserAuthorities() {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        Collection<? extends GrantedAuthority> authorities = securityContext.getAuthentication().getAuthorities();

        return authorities;

    }

    public static String getCurrentLogin(){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        UserDetails springSecurityUser = null;
        String userName = null;
        if(authentication != null){
            if(authentication.getPrincipal() instanceof UserDetails){
                springSecurityUser = (UserDetails) authentication.getPrincipal();
                userName = springSecurityUser.getUsername();
            } else if(authentication.getPrincipal() instanceof String){
                userName = (String) authentication.getPrincipal();
            }
        }
        return userName;
    }

}
