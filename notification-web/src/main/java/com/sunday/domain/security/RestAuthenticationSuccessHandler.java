package com.sunday.domain.security;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.User;
import com.sunday.domain.service.UserServiceLocale;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * Created by michal on 11.12.15.
 */
@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    public static final Logger log = LoggerFactory.getLogger(RestAuthenticationSuccessHandler.class);

    @Inject
    UserServiceLocale userServiceLocale;

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request,
                                        final HttpServletResponse response, final Authentication authentication)
            throws ServletException, IOException {
        log.info("onAuthenticationSuccess ");

        try {
            User result = userServiceLocale.findUserWithAuthorities(request.getParameter("username"));
            UserDto user = result.getUserInfoWithAuthorities();
            response.setContentType("application/json");
            response.getWriter().print("{\"isSuccess\" : true, " +
                    "\"userId\" : "+user.getUserId()+","+
                    "\"username\" : \"" + user.getEmail() + "\", "+
        "\"roles\" : \""+user.getAuthorities()+"\" }");
            response.getWriter().flush();
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }



    }


}
