package com.sunday.domain.security;

import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.Authority;
import com.sunday.domain.model.User;
import com.sunday.domain.service.UserServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 29.11.15.
 */
public class NotificationUserDetailsService implements UserDetailsService {

    public static final Logger log = LoggerFactory.getLogger(NotificationUserDetailsService.class);

    @Inject
    UserServiceRemote userService;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            User user = userService.findUserWithAuthorities(email);

            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            for(Authority a : user.getAuthorities()){
                authorities.add(new SimpleGrantedAuthority(a.getName()));
            }

            SimpleUserDetails simpleUserDetails = new SimpleUserDetails(user.getEmail(), user.getPassword(), authorities);
            return simpleUserDetails;
        }    catch (UserNotFoundException e){
            throw new UsernameNotFoundException(email);
        }
    }
}
