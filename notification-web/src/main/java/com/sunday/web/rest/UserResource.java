package com.sunday.web.rest;

import com.sunday.domain.dto.Paginator;
import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.User;
import com.sunday.web.rest.dto.CreateUserDto;
import com.sunday.web.rest.dto.ErrorDto;
import org.slf4j.Logger;
import com.sunday.domain.service.UserServiceRemote;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.awt.*;


/**
 * Created by michal on 23.11.15.
 */

@RestController
@RequestMapping(value = "/admin/users")
public class UserResource {

    private static final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    UserServiceRemote userservice;

    @Inject
    PasswordEncoder passwordEncoder;

    @Inject
    MessageSource messageSource;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@Valid @RequestBody CreateUserDto createUserDto) {
        log.info("Create user : " + createUserDto);

        try {
            User existingUser = userservice.findUserByEmail(createUserDto.getEmail());

        } catch (UserNotFoundException e) {
            User createUser = new User(createUserDto.getEmail(), createUserDto.getFirstName(), createUserDto.getLastName(), passwordEncoder.encode(createUserDto.getPassword()));
            UserDto userDto = userservice.createUser(createUser);

            log.info("After create user: " + createUser);
            return new ResponseEntity<UserDto>(userDto, HttpStatus.CREATED);
        }

        String message = messageSource.getMessage("User.EmailExist", new Object[]{
                createUserDto.getEmail()}
                , null);

        ErrorDto errorDto = new ErrorDto(message);

        return new ResponseEntity<ErrorDto>(errorDto, HttpStatus.CONFLICT);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<UserDto> getUser(@PathVariable(value = "userId") int userId) {
        log.debug("get userId: " + userId);
        UserDto findUser = userservice.findUser(userId);

        return new ResponseEntity<UserDto>(findUser, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<Paginator<UserDto>> getAllUser(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                                  @RequestParam(value = "limit", required = false, defaultValue = "15") int limit) {
        log.debug("Get all user ");
        int offset = (page - 1) * limit;
        Paginator<UserDto> result = userservice.getAllUser(offset, limit);

        return new ResponseEntity<Paginator<UserDto>>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}")
    public
    @ResponseBody
    ResponseEntity<Void> deleteUser(@PathVariable(value = "userId") int userId) {

        userservice.deleteUser(userId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }


}
