package com.sunday.web.rest.dto;

import java.io.Serializable;
import java.util.List;


public class ValidDto implements Serializable {

    List<FieldDto> errors;

    public ValidDto(List<FieldDto> errors) {
        this.errors = errors;
    }

    public static class FieldDto {
        private String field;
        private String error;

        public FieldDto(String field, String error) {
            this.field = field;
            this.error = error;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }


}
