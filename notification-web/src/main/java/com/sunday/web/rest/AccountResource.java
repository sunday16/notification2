package com.sunday.web.rest;

import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.User;
import com.sunday.domain.security.SecurityUtils;
import com.sunday.domain.service.UserServiceRemote;
import com.sunday.web.rest.dto.ChangePasswordDto;
import com.sunday.web.rest.dto.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/admin/account")
public class AccountResource {

    private static final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    UserServiceRemote userService;

    @Inject
    PasswordEncoder passwordEncoder;

    @Inject
    MessageSource messageSource;


    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getAccount() {
        log.info("getAccount: ");
        User user = null;
        try {
            String currentLogin = SecurityUtils.getCurrentLogin();
            user = userService.findUserWithAuthorities(currentLogin);
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }

        UserDto result = user.getUserInfoWithAuthorities();

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/change_password")
    public ResponseEntity<?> changePassword(@Valid @RequestBody ChangePasswordDto changePasswordDto) {

        log.info("Change password: ");
        try {
            String currentLogin = SecurityUtils.getCurrentLogin();
            User user = userService.findUserByEmail(currentLogin);

            boolean match = passwordEncoder.matches(changePasswordDto.getActualPassword(), user.getPassword());

            if (match) {

                String newPasswordEncoded = passwordEncoder.encode(changePasswordDto.getNewPassword());
                userService.changePassword(newPasswordEncoded, user);
            } else {
                String message = messageSource.getMessage("User.ActualPassword.NotMatch", null, null);
                ErrorDto errorDto = new ErrorDto(message);
                return new ResponseEntity<ErrorDto>(errorDto, HttpStatus.BAD_REQUEST);
            }


        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
