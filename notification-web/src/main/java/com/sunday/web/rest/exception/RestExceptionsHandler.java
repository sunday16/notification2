package com.sunday.web.rest.exception;

import com.sunday.web.rest.dto.FieldErrorDto;
import com.sunday.web.rest.dto.ValidationErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 27.11.15.
 */
@ControllerAdvice
public class RestExceptionsHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ValidationErrorDto processValidationError(MethodArgumentNotValidException ex) {

        BindingResult result = ex.getBindingResult();

        List<FieldError> fieldErrors = result.getFieldErrors();
        List<FieldErrorDto> errorDtos = new ArrayList<>(fieldErrors.size());
        for (FieldError error : fieldErrors) {
            errorDtos.add(new FieldErrorDto(error.getField(), error.getDefaultMessage()));
        }

        return new ValidationErrorDto(errorDtos);

    }
}
