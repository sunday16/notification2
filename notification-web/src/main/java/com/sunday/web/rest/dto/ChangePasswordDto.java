package com.sunday.web.rest.dto;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by michal on 09.01.16.
 */
public class ChangePasswordDto {

    @NotNull
    @NotEmpty
    private String actualPassword;

    @NotNull
    @NotEmpty
    private String newPassword;

    public ChangePasswordDto() {
    }

    public String getActualPassword() {
        return actualPassword;
    }

    public void setActualPassword(String actualPassword) {
        this.actualPassword = actualPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "ChangePasswordDto{" +
                "actualPassword='" + actualPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
