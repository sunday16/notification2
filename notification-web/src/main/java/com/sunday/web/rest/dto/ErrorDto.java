package com.sunday.web.rest.dto;

import java.io.Serializable;

/**
 * Created by michal on 09.01.16.
 */
public class ErrorDto implements Serializable {

    private String message;

    public ErrorDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorDto{" +
                "message='" + message + '\'' +
                '}';
    }
}
