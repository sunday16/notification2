package com.sunday.web.rest.dto;

import java.util.List;

/**
 * Created by michal on 27.11.15.
 */
public class ValidationErrorDto {
    List<FieldErrorDto> errors;

    public ValidationErrorDto() {
    }

    public ValidationErrorDto(List<FieldErrorDto> errors) {
        this.errors = errors;
    }

    public List<FieldErrorDto> getErrors() {
        return errors;
    }

    public void setErrors(List<FieldErrorDto> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "ValidationErrorDto{" +
                "errors=" + errors +
                '}';
    }
}
