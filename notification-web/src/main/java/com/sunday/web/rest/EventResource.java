package com.sunday.web.rest;

import com.sunday.domain.dto.EventDto;
import com.sunday.domain.dto.Paginator;
import com.sunday.domain.model.Address;
import com.sunday.domain.model.Event;
import com.sunday.domain.model.LatLon;
import com.sunday.domain.security.SecurityUtils;
import com.sunday.domain.service.EventServiceRemote;
import com.sunday.web.rest.dto.CreateEventDto;
import com.sunday.web.rest.dto.EditEventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * Created by michal on 27.11.15.
 */
@RestController
@RequestMapping(value = "/admin/events")
public class EventResource {

    public static final Logger log = LoggerFactory.getLogger(EventResource.class);

    @Inject
    EventServiceRemote eventService;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<EventDto> createEvent(@Valid @RequestBody CreateEventDto createEventDto) {
        Address address = Address.valueOf(createEventDto.getCountry(), createEventDto.getState(),
                createEventDto.getCity(), createEventDto.getStreet(), createEventDto.getNumber());
        LatLon latLon = LatLon.valueOf(createEventDto.getLat(), createEventDto.getLon());
        Event createEvent = new Event(createEventDto.getName(),
                createEventDto.getShortDescr(),
                createEventDto.getLongDescr(),
                createEventDto.getOrganizer(),
                createEventDto.getDateEvent(),
                address,
                latLon);
        EventDto eventDto = eventService.createEvent(createEvent);
        return new ResponseEntity<EventDto>(eventDto, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/edit/{id}")
    public ResponseEntity<EventDto> updateEvent( @PathVariable(value = "id") long id,
                                                @Valid @RequestBody EditEventDto editEventDto) {
        log.info("Update event: ");
        EventDto eventDto = eventService.findEvent(id);
        eventDto.setName(editEventDto.getName());
        eventDto.setShortDescr(editEventDto.getShortDescr());
        eventDto.setLongDescr(editEventDto.getLongDescr());
        eventDto.setOrganizer(editEventDto.getOrganizer());
        eventDto.setDateEvent(editEventDto.getDateEvent());
        eventDto.setCountry(editEventDto.getCountry());
        eventDto.setState(editEventDto.getState());
        eventDto.setCity(editEventDto.getCity());
        eventDto.setStreet(editEventDto.getStreet());
        eventDto.setNumber(editEventDto.getNumber());
        eventDto.setLat(editEventDto.getLat());
        eventDto.setLon(editEventDto.getLon());
        Address address = Address.valueOf(eventDto.getCountry(), eventDto.getState(),
                eventDto.getCity(), eventDto.getStreet(), eventDto.getNumber());
        LatLon latLon = LatLon.valueOf(eventDto.getLat(), eventDto.getLon());
        Event createEvent = new Event(eventDto.getName(),
                eventDto.getShortDescr(),
                eventDto.getLongDescr(),
                eventDto.getOrganizer(),
                eventDto.getDateEvent(),
                address,
                latLon);
        eventDto = eventService.updateEvent(id, createEvent);
        return new ResponseEntity<EventDto>(eventDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<EventDto> getEvent(@PathVariable(value = "id") long id) {
        EventDto result = eventService.findEvent(id);

        return new ResponseEntity<EventDto>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Paginator<EventDto>> getAllEvent(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                                           @RequestParam(value = "limit", required = false, defaultValue = "5") int limit) {

        int offset = (page - 1) * limit;
        Paginator<EventDto> result = eventService.getAllEvent(offset, limit);

        return new ResponseEntity<Paginator<EventDto>>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable(value = "id") long id) {
        eventService.deleteEvent(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
