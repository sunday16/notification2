package com.sunday.web.rest.dto;

import java.nio.charset.Charset;

/**
 * Created by michal on 27.11.15.
 */
public class FieldErrorDto {



    private String name;
    private String errorDescription;

    public FieldErrorDto(String name, String errorDescription) {
        this.name = name;
        this.errorDescription = errorDescription;
    }

    public FieldErrorDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {

        this.errorDescription = errorDescription;
    }



    @Override
    public String toString() {
        return "FieldErrorDto{" +
                "name='" + name + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
