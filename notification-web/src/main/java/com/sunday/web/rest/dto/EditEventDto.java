package com.sunday.web.rest.dto;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by michal on 27.11.15.
 */
public class EditEventDto {

    @Length(min = 4, max = 80)
    @NotNull
    private String name;

    @Length(min = 10, max = 255)
    @NotNull
    private String shortDescr;

    @Length(min = 10)
    @NotNull
    private String longDescr;

    @Length(min = 3, max = 120)
    @NotNull
    private String organizer;

    @NotNull
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateEvent;

    @NotNull
    @Length(min = 3, max = 120)
    private String country;

    @NotNull
    @Length(min = 3)
    private String state;

    @NotNull
    @Length(min = 4, max = 255)
    private String city;

    @NotNull
    @Length(min = 3, max = 255)
    private String street;

    @NotNull
    @Length(min = 1, max = 20)
    private String number;

    @DecimalMax(value = "90")
    @DecimalMin(value = "-90")
    private double lat;

    @DecimalMax(value = "180")
    @DecimalMin(value = "-180")
    private double lon;

    public EditEventDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public void setShortDescr(String shortDescr) {
        this.shortDescr = shortDescr;
    }

    public String getLongDescr() {
        return longDescr;
    }

    public void setLongDescr(String longDescr) {
        this.longDescr = longDescr;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "CreateEventDto{" +
                "name='" + name + '\'' +
                ", shortDescr='" + shortDescr + '\'' +
                ", longDescr='" + longDescr + '\'' +
                ", organizer='" + organizer + '\'' +
                ", dateEvent=" + dateEvent +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
