package com.sunday.web.rest;

import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.web.rest.dto.ErrorDto;
import com.sunday.web.rest.dto.FieldErrorDto;
import com.sunday.web.rest.dto.ValidDto;
import com.sunday.web.rest.dto.ValidationErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@ControllerAdvice
public class ExceptionHandlerAdvice {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

    @Inject
    private MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
        BindingResult result = error.getBindingResult();
        List<FieldError> fieldError = result.getFieldErrors();
        List<FieldErrorDto> fieldErrorDtos = new ArrayList<>(fieldError.size());
        for (FieldError f : fieldError) {
            String message = resolveErrorMessage(f);

            fieldErrorDtos.add(new FieldErrorDto(f.getField(), message));
        }

        ValidationErrorDto validationErrorDto = new ValidationErrorDto(fieldErrorDtos);

        return new ResponseEntity<ValidationErrorDto>(validationErrorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ErrorDto> handleUserNotFoundException(UsernameNotFoundException error){


        final Object[] arguments = {error.getMessage()};
        String message = messageSource.getMessage("Username.Not.Found", arguments,null);

        ErrorDto errorDto = new ErrorDto(message);

        return new ResponseEntity<ErrorDto>(errorDto,HttpStatus.BAD_REQUEST);
    }

    private String resolveErrorMessage(FieldError f) {

        String localizedMessage = f.getDefaultMessage();
        for (String code : f.getCodes()) {
            String message = messageSource.getMessage(code, f.getArguments(), null);
            if (message != code) {
                localizedMessage = message;
                break;
            }
        }
        return localizedMessage;
    }


}
