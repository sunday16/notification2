/**
 *
 * Created by michal on 07.01.16.
 */
angular.module('notificationApp')
    .controller('NavbarController', ['$scope', 'principal', '$rootScope', function ($scope, principal) {

        $scope.isAuthenticated = principal.isAuthenticated;

    }]);
