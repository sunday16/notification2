/**
 * Created by michal on 03.01.16.
 */
angular.module('notificationApp')
    .factory('principal', ['$q', '$http', '$window','$timeout',
        function ($q, $http,$window , $timeout) {
            var _identity = undefined,
                _authenticated = false;

            return {
                isIdentityResolved: function () {
                    return angular.isDefined(_identity);
                },
                isAuthenticated: function () {
                    return _authenticated;
                },
                isInRole: function (roles) {
                    if (!_authenticated || !_identity.roles) return false;
                    return this.identity().then(function(_id){
                       return _id.roles && _id.roles.indexOf(roles) != -1;
                    }, function(err){
                        return false;
                    });
                },
                isInAnyRole: function (roles) {
                    if (!_authenticated || !_identity.roles) return false;

                    for (var i = 0; i < roles.length; i++) {
                        if (this.isInRole(roles[i])) return true;
                    }

                    return false;
                },

                authenticate: function (identity) {
                    _identity = identity;
                    _authenticated = identity != null;


                    /*if (identity) localStorage.setItem("identity", angular.toJson(identity));
                    else localStorage.removeItem("identity");*/
                },
                identity: function (force) {
                    var deferred = $q.defer();

                    if (force === true) _identity = undefined;

                    // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
                    if (angular.isDefined(_identity)) {
                        deferred.resolve(_identity);

                        return deferred.promise;
                    }


                    $http.get('/admin/account', {ignoreErrors: true})
                        .success(function (data) {
                            var user = data;
                            _identity = {
                                'username': user.email,
                                'roles': user.authorities,
                                'userId' : user.userId
                            };
                            _authenticated = true;
                            deferred.resolve(_identity);
                        })
                        .error(function () {
                            _identity = null;
                            _authenticated = false;
                            deferred.resolve(_identity);
                        });


                    return deferred.promise;
                },

                changePassword: function(actualPassword,newPassword){
                    var defferer = $q.defer();

                    var passwordData = {
                        'actualPassword': actualPassword,
                        'newPassword': newPassword
                    };
                    $http.post('/admin/account/change_password',passwordData)
                        .success(function(data){
                            defferer.resolve(data);
                        })
                        .error(function(error,status){

                            var result = {
                                error : error,
                                status: status
                            };

                            defferer.reject(result)
                        });
                    return defferer.promise;
                }
            };
        }
    ]);