/**
 * Created by michal on 07.01.16.
 */
angular.module('notificationApp')
.controller('ChangePasswordCtrl',['$scope','principal',function($scope,principal){
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.errorActualPassword = null;
        $scope.changePassword = function(){
            if($scope.password !== $scope.confirmPassword){
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.doNotMatch = null;
                principal.changePassword($scope.actualPassword, $scope.password).then(function(){
                    $scope.error = null;
                    $scope.success = 'OK';
                }).catch(function(error){
                    $scope.success = null;

                    if(error.status === 400){
                        $scope.errorActualPassword = 'ERROR'
                    }else {
                        $scope.error = 'ERROR';
                    }
                });
            }
        }
    }]);