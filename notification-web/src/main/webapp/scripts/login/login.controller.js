
angular.module('notificationApp')

    .controller('LoginController', ['$scope', 'authFactory', '$state','principal','$timeout',
        function ($scope, authFactory, $state , principal, $timeout) {
        $scope.credentials = {};
        $scope.userPost = {};

        $scope.login = function (credentials) {
            authFactory.login(credentials).then(function(data){
                $scope.userPost = data;
                    $scope.authenticationError = false;



                if ($scope.userPost.isSuccess == true) {
                    principal.authenticate($scope.userPost);

                    if ($scope.returnToState) $state.go($scope.returnToState.name, $scope.returnToStateParams);
                    else $state.go('login');
                    $state.go("events");
                }else{
                    $scope.authenticationError = true;
                    $timeout(function(){
                        $scope.authenticationError = false;
                    },5000);
                    $scope.error = 'Error';
                }}

            , function(){
                    $scope.authenticationError = true;
                    $timeout(function(){
                        $scope.authenticationError = false;
                    },5000);
                    $scope.error = 'Error';
            })

        };




    }])
    .factory('authFactory', [ '$http', '$q', function ( $http, $q) {

        return {

            login: function (user) {
                var deffered = $q.defer();

                var data = 'username=' + encodeURIComponent(user.username) +
                    '&password=' + encodeURIComponent(user.password);
                $http.post('/login', data, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                    .success(function (data) {

                        deffered.resolve(data);

                    }).error(function (error) {
                        deffered.reject(error);

                    });

                return deffered.promise;
            }
        };
    }
        ]
);
