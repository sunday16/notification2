/**
 * Created by michal on 07.01.16.
 */
angular.module('notificationApp')
    .directive('isInRole', ['principal', function (principal) {
        return {
            restrict: 'A',
            link: function ($scope, element, attrs) {
                var setVisible = function () {
                        element.removeClass('hidden');
                    },
                    setHidden = function () {
                        element.addClass('hidden');
                    },
                    defineVisibility = function (reset) {

                        if (reset) {
                            setVisible();
                        }

                        principal.isInRole(authority)
                            .then(function (result) {
                                if (result) {
                                    setVisible();
                                } else {
                                    setHidden();
                                }
                            });
                    },
                    authority = attrs.isInRole.replace(/\s+/g, '');

                if (authority.length > 0) {
                    defineVisibility(true);
                }
            }
        };
    }]);