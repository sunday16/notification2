/**
 * Created by michal on 03.01.16.
 */

angular.module('notificationApp')
    .controller('LogoutCtrl', ['$scope', '$state', 'principal', '$http', function ($scope, $state, principal, $http) {

        $scope.logout = function () {
            $http.post("/logout", {})
                .success(function (data) {
                    principal.authenticate(null);

                    if (localStorage.getItem("identity")) {
                           localStorage.removeItem("identity");
                    }
                    $state.go("login");
                    return data;
                }
            )
        }
    }
    ])
;


