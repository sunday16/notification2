/**
 * Created by michal on 09.12.15.
 */
angular.module('notificationApp')
    .config(['$stateProvider', '$urlRouterProvider','$httpProvider', function ($stateProvider, $urlRouterProvider,$httpProvider) {
        //For any unmatched url, redirect to /login
        $httpProvider.defaults.withCredentials = true;
        $urlRouterProvider.otherwise('/login');

        //Set up the state
        $stateProvider
            .state('site', {
                abstract: true,
                templateUrl: "/scripts/app.html",
                controller: 'NavbarController',
                resolve: {
                    authorize: ['authorization',
                        function(authorization) {
                            return authorization.authorize();
                        }
                    ]
                }
            })
            .state('login', {
                url: "/login",
                templateUrl: "/scripts/login/login.html",
                controller: 'LoginController',
                data: {
                    roles: []
                }
            })
            .state('logout', {
                url: "/login?logout",
                templateUrl: "/scripts/login/login.html",
                controller: 'LogoutCtrl',
                data : {
                    roles: []
                }

            })
            .state('denied',{
                parent : 'site',
                url : "/denied",
                templateUrl: "scripts/login/denied.html",
                data : {
                    roles: []
                }

            })
            .state('users', {
                parent: 'site',
                url: "/admin/users",
                templateUrl: "/scripts/user/user.html",
                controller: 'UserController',
                data : {
                    roles : ['ROLE_ADMIN']
                }
            })
            .state('events', {
                parent: 'site',
                url: "/admin/events",
                templateUrl: '/scripts/event/event.html',
                controller: 'EventController',
                data : {
                    roles : ['ROLE_ADMIN','ROLE_USER']
                }
            })
            .state('eventDetail', {
                parent: 'site',
                url: "/admin/events/:id",
                templateUrl: '/scripts/event/event.detail.html',
                controller: 'EventDetailCtrl',
                data : {
                    roles : ['ROLE_ADMIN','ROLE_USER']
                }
            })
            .state('changePassword',{
                parent: 'site',
                url: "/admin/account/change_password",
                templateUrl: '/scripts/login/change_password.html',
                controller: 'ChangePasswordCtrl',
                data : {
                    roles : ['ROLE_ADMIN','ROLE_USER']
                }
            })
    }])
    .run(['$rootScope', '$state', '$stateParams', 'authorization', 'principal',
        function($rootScope, $state, $stateParams, authorization, principal) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
                // track the state the user wants to go to; authorization service needs this
                $rootScope.toState = toState;
                $rootScope.toStateParams = toStateParams;
                // if the principal is resolved, do an authorization check immediately. otherwise,
                // it'll be done when the state it resolved.
                if (principal.isIdentityResolved()) authorization.authorize();
            });
        }
    ])
    .config(
    ['uiGmapGoogleMapApiProvider', function (GoogleMapApiProviders) {
        GoogleMapApiProviders.configure({
            china: true
        });
    }]
);
