/**
 * Created by michal on 09.12.15.
 */
angular.module('notificationApp',[
    'ui.router',
    'angularUtils.directives.dirPagination',
    'ui.bootstrap',
    'srph.timestamp-filter',
    'uiGmapgoogle-maps',
    'wysiwyg.module',
    'colorpicker.module',
    'angular-redactor',
    'ngAnimate',
    'ngSanitize'
]);
