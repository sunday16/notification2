/**
 * Created by michal on 20.12.15.
 */
angular.module('notificationApp')
    .controller('EventDetailCtrl', function ($scope, $stateParams, EventService) {
        $scope.event = {};
        function load(id) {
            EventService.getEvent(id).then(function (data) {
                $scope.event = data;
                $scope.map = {
                    center: {
                        latitude: $scope.event.lat,
                        longitude: $scope.event.lon
                    },
                    zoom: 12,
                    options: {
                        scrollwheel: false
                    }

                };
                $scope.marker = {
                    id: 0,
                    coords: {
                        latitude: $scope.event.lat,
                        longitude: $scope.event.lon
                    }
                }
            })
        }

        load($stateParams.id);


    })
    .filter('timestampToDate', function () {
        return function (timestamp) {
            var date = new Date(timestamp);
            var dateObject = ('0' + date.getDate()).slice(-2)+ '.'+ ('0' + (date.getMonth() + 1)).slice(-2)+'.'+date.getFullYear();
            return dateObject;
        };
    })

    .filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        }
    }]);