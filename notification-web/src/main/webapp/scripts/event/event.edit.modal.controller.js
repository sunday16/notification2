/**
 * Created by michal on 21.12.15.
 */
angular.module('notificationApp')
    .controller('EventEditModalCtrl', ['$scope', '$modalInstance', 'EventService', 'editId',
        function ($scope, $modalInstance, EventService, editId) {
            /*$scope.map = { center: { latitude: 0, longitude: 0 }, zoom: 8 };*/
            $scope.dateNow = addDays(new Date(),1);
            load(editId);
            $scope.error = null;
            $scope.customMenu = [
                ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
                ['format-block'],
                ['font'],
                ['font-size'],
                ['font-color', 'hilite-color'],
                ['remove-format'],
                ['ordered-list', 'unordered-list', 'outdent', 'indent'],
                ['left-justify', 'center-justify', 'right-justify']

            ];

            function load(id){
                EventService.getEvent(id).then(function(data){
                    $scope.eventForm = data;
                    $scope.lat = $scope.eventForm.lat;
                    $scope.lon = $scope.eventForm.lon;



                    $scope.eventForm.dateEvent = new Date($scope.eventForm.dateEvent);
                    $scope.map = {
                        center: {
                            latitude: $scope.eventForm.lat,
                            longitude: $scope.eventForm.lon
                        },
                        zoom: 12,
                        options: {
                            scrollwheel: false
                        },
                        markers: [],
                        events: {
                            click: function (mapModel, eventName, originalEventArgs) {
                                var e = originalEventArgs[0];
                                var marker = {};
                                marker.id = 1;
                                marker.coords = {};

                                marker.coords.latitude = e.latLng.lat().toFixed(6);
                                marker.coords.longitude = e.latLng.lng().toFixed(6);
                                $scope.lat = parseFloat(e.latLng.lat().toFixed(6));
                                $scope.lon = parseFloat(e.latLng.lng().toFixed(6));
                                $scope.map.markers.pop();
                                $scope.map.markers.push(marker);
                                $scope.$apply();
                            }
                        }

                    };
                    $scope.marker = {
                        id: 0,
                        coords: {
                            latitude: $scope.eventForm.lat,
                            longitude: $scope.eventForm.lon
                        }
                    };
                    $scope.map.markers.push($scope.marker);

                });
            }




            $scope.save = function () {
                var formData =
                {
                    "name": $scope.eventForm.name,
                    "shortDescr": $scope.eventForm.shortDescr,
                    "longDescr": $scope.eventForm.longDescr,
                    "organizer": $scope.eventForm.organizer,
                    "dateEvent": Date.parse($scope.eventForm.dateEvent),
                    "country": $scope.eventForm.country,
                    "state": $scope.eventForm.state,
                    "city": $scope.eventForm.city,
                    "street": $scope.eventForm.street,
                    "number": $scope.eventForm.number,
                    "lat": $scope.lat,
                    "lon": $scope.lon
                };


                EventService.updateEvent(editId, formData)
                    .then(function () {
                        $modalInstance.close();
                    }, function (error) {
                        if(error.status === 400){
                            $scope.error = true;
                            $scope.errorMessages = error.error.errors;
                        }

                    })
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function addDays(theDate, days) {
                return new Date(theDate.getTime() + days*24*60*60*1000);
            };

            $scope.clear = function() {
                $scope.eventForm.dateEvent = null;
            };

            // Disable weekend selection
            $scope.disabled = function(date, mode) {
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            };

            $scope.open2 = function() {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function(year, month, day) {
                $scope.eventFormDateEvent = new Date(year, month, day);
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[2];


            $scope.popup2 = {
                opened: false
            };
        }]);
