/**
 * Created by michal on 20.12.15.
 */
angular.module('notificationApp')
    .factory('EventService', ['$http', '$q', function ($http, $q) {
        return {
            getEvents: function (pageNumber, resultPerPage) {
                var defferef = $q.defer();

                $http({
                    method: 'GET',
                    url: '/admin/events',
                    params: {"page": pageNumber, "limit": resultPerPage}
                }).success(function (data) {
                    return defferef.resolve(data);
                }).error(function (data) {
                    return reject(data);
                });
                return defferef.promise;
            },

            addEvent: function (event) {
                var defferer = $q.defer();

                $http.post('/admin/events', event)
                    .success(function (data) {
                        return defferer.resolve(data);
                    })
                    .error(function (error,status) {
                        var result = {
                            error: error,
                            status : status
                        };
                        return defferer.reject(result);
                    });
                return defferer.promise;
            },

            getEvent: function (id) {
                var defferer = $q.defer();
                $http({
                    method: 'GET',
                    url: '/admin/events/' + id
                }).success(function (data) {
                    return defferer.resolve(data);
                }).error(function (data) {
                    return defferer.reject(data);
                });
                return defferer.promise;
            },

            updateEvent: function (id, data) {
                var defferer = $q.defer();

                $http.put("/admin/events/edit/" + id, data)
                    .success(function (data) {
                        return defferer.resolve(data);
                    })
                    .error(function (error,status) {
                        var result = {
                            error: error,
                            status : status
                        };
                        return defferer.reject(result);
                    });
                return defferer.promise;
            },

            deleteEvent: function(id){
                var defferer = $q.defer();

                $http.delete("/admin/events/"+id)
                    .success(function (data){
                    return defferer.resolve(data);
                })
                    .error(function (error){
                        return defferer.reject(error);
                    });
                return defferer.promise;
            }
        }
    }]);