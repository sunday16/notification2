/**
 * Created by michal on 30.12.15.
 */
angular.module('notificationApp')
    .controller('EventModalCtrl', ['$scope', '$modalInstance', 'EventService', '$timeout', function ($scope, $modalInstance, EventService, $timeout) {
        /*$scope.map = { center: { latitude: 0, longitude: 0 }, zoom: 8 };*/
        $scope.lat = 0;
        $scope.lon = 0;
        $scope.render = true;
        $scope.dateNow = addDays(new Date(),1);


        $scope.error = null;


        $scope.customMenu = [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['format-block'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify']

        ];

        $scope.map = {
            center: {
                latitude: 52.188810,
                longitude: 19.532376
            },
            control: {
                refresh: {latitude: $scope.lat, longitude: $scope.lon}
            },
            zoom: 6,
            markers: [],
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    var e = originalEventArgs[0];
                    var marker = {};
                    marker.id = 1;
                    marker.coords = {};

                    marker.coords.latitude = e.latLng.lat().toFixed(6);
                    marker.coords.longitude = e.latLng.lng().toFixed(6);
                    $scope.lat = parseFloat(e.latLng.lat().toFixed(6));
                    $scope.lon = parseFloat(e.latLng.lng().toFixed(6));
                    $scope.map.markers.pop();
                    $scope.map.markers.push(marker);
                    $scope.$apply();
                }

            }
        };

        $scope.save = function () {
            var formData =
            {
                "name": $scope.eventFormName,
                "shortDescr": $scope.eventFormShortDescr,
                "longDescr": $scope.eventFormLongDescr,
                "organizer": $scope.eventFormOrganizer,
                "dateEvent": Date.parse($scope.eventFormDateEvent),
                "country": $scope.eventFormCountry,
                "state": $scope.eventFormState,
                "city": $scope.eventFormCity,
                "street": $scope.eventFormStreet,
                "number": $scope.eventFormNumber,
                "lat": $scope.lat,
                "lon": $scope.lon
            };


            EventService.addEvent(formData)
                .then(function () {
                    $modalInstance.close();
                }, function (error) {
                    if(error.status === 400){
                        $scope.error = true;
                        $scope.errorMessages = error.error.errors;
                    }

                })
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        function addDays(theDate, days) {
            return new Date(theDate.getTime() + days*24*60*60*1000);
        };


        $scope.clear = function() {
            $scope.eventFormDateEvent = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };

        $scope.setDate = function(year, month, day) {
            $scope.eventFormDateEvent = new Date(year, month, day);
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[2];

        $scope.popup2 = {
            opened: false
        };


    }]);


