/**
 * Created by michal on 01.01.16.
 */
angular.module('notificationApp')
    .controller('EventDeleteModalCtrl', ['$scope', '$modalInstance', 'EventService', 'Id',
        function ($scope, $modalInstance, EventService, Id) {

            $scope.delete = function () {
                EventService.deleteEvent(Id)
                    .then(function () {
                        $modalInstance.close();
                    })
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);