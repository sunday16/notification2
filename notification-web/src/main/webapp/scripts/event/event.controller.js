/**
 * Created by michal on 09.12.15.
 */
angular.module('notificationApp')
    .controller('EventController', ['$scope', '$http', 'EventService', '$uibModal',
        function ($scope, $http, EventService, $uibModal) {
            $scope.events = {};
            $scope.totalEvents = 0;
            $scope.eventsPerPage = 8;

            getResultPage(1, $scope.eventsPerPage);


            $scope.pagination = {
                current: 1
            };

            $scope.pageChanged = function (newPage) {
                getResultPage(newPage, $scope.eventsPerPage);
            };

            function getResultPage(pageNumber, resultPerPage) {
                EventService.getEvents(pageNumber, resultPerPage)
                    .then(function (data) {
                        $scope.events = data.allResult;
                        angular.forEach($scope.events, function (value, key) {
                            var date = new Date(value.dateEvent);
                            value.dateEvent = date.getFullYear() + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2);
                        });
                        $scope.totalEvents = data.total;
                    }, function (error) {
                        console.log('Error events',error);
                    })
            }


            $scope.add = function (size) {
                var modalInstance = $uibModal.open({
                    size: size,
                    scope: $scope,
                    animation: false,
                    backdrop: 'true',
                    templateUrl: 'scripts/event/event.modal.tmp.html',
                    controller: 'EventModalCtrl',
                    resolve: {}
                });
                modalInstance.result.then(function () {
                    getResultPage($scope.pagination.current, $scope.eventsPerPage);
                });

            };


            $scope.edit = function (size, id) {
                var modalInstance = $uibModal.open({
                    size: size,
                    scope: $scope,
                    animation: false,
                    backdrop: 'true',
                    templateUrl: '/scripts/event/event.edit.modal.tmp.html',
                    controller: 'EventEditModalCtrl',
                    resolve: {
                        editId: function () {
                            return id;
                        }
                    }
                })
            };

            $scope.delete = function(size, id) {
                var modalInstance = $uibModal.open({
                    size: size,
                    scope: $scope,
                    animation: false,
                    backdrop: 'true',
                    templateUrl: '/scripts/event/event.delete.tmp.html',
                    controller: 'EventDeleteModalCtrl',
                    resolve: {
                        Id: function() {
                            return id;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    getResultPage($scope.pagination.current, $scope.eventsPerPage);
                });
            }
        }]);
