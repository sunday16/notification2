/**
 * Created by michal on 09.12.15.
 */
angular.module('notificationApp')
    .controller('UserController', ['$scope', '$http', 'userService', '$uibModal', function ($scope, $http, userService, $uibModal) {
        $scope.users = [];
        $scope.totalUsers = 0;
        $scope.usersPerPage = 8;
        $scope.error = null;
        $scope.errorUserExist = null;


        getResultPage(1, $scope.usersPerPage);

        $scope.pagination = {
            current: 1
        };

        $scope.pageChanged = function (newPage) {
            getResultPage(newPage, $scope.usersPerPage);
        };

        function getResultPage(pageNumber, resultPerPage) {
            userService.getUsers(pageNumber, resultPerPage).then(function (data) {
                $scope.users = data.allResult;
                $scope.totalUsers = data.total;
            }, function () {
            });

        }

        $scope.add = function (size) {
            var modalInstance = $uibModal.open({
                size: size,
                scope: $scope,
                animation: false,
                backdrop: 'true',
                templateUrl: 'scripts/user/user.modal.tmp.html',
                controller: 'UserModalCtrl',
                resolve: {}
            });
            modalInstance.result.then(function () {
                getResultPage($scope.pagination.current, $scope.usersPerPage);

            })
        };

        $scope.delete = function (size, id) {
            var modalInstance = $uibModal.open({
                size: size,
                scope: $scope,
                animation: false,
                backdrop: 'true',
                templateUrl: '/scripts/user/user.delete.modal.tmp.html',
                controller: 'UserDeleteModalCtrl',
                resolve: {
                    Id: function () {
                        return id;
                    }
                }
            });
            modalInstance.result.then(function () {
                getResultPage($scope.pagination.current, $scope.usersPerPage);
            });
        }


    }])

    .controller('UserModalCtrl', ['$scope', '$modalInstance', 'userService', function ($scope, $modalInstance, userService) {

        $scope.save = function () {
            var formData =
            {
                "userId": $scope.users.userId,
                "firstName": $scope.userFormFirstName,
                "lastName": $scope.userFormLastName,
                "email": $scope.userFormEmail,
                "password": $scope.userFormPassword
            };
            userService.addUser(formData)
                .then(function () {
                    $modalInstance.close();
                }, function (error) {
                    if(error.status === 400){
                        $scope.error = true;
                        $scope.errorMessages = error.error.errors;
                    }else if(error.status === 409){
                        $scope.errorUserExist = true;

                        $scope.errorEmailMessage = error.error.message;

                    }
                })
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
    }]);


