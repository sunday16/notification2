/**
 * Created by michal on 06.01.16.
 */
angular.module('notificationApp')
    .controller('UserDeleteModalCtrl', ['$scope', '$modalInstance', 'userService', 'Id',
        function ($scope, $modalInstance, userService, Id) {

            $scope.delete = function () {
                userService.deleteUser(Id)
                    .then(function () {
                        $modalInstance.close();
                    })
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            }
        }]);