/**
 * Created by michal on 06.01.16.
 */
angular.module('notificationApp')
    .factory('userService', ['$http', '$q', function ($http, $q) {

        return {
            getUsers: function (pageNumber, resultPerPage) {
                var defferer = $q.defer();

                $http({
                    method: 'GET',
                    url: '/admin/users',
                    params: {"page": pageNumber, "limit": resultPerPage}
                }).success(function (data) {
                    defferer.resolve(data);
                })
                    .error(function (data) {
                        defferer.reject(data)
                    });

                return defferer.promise;
            },

            addUser: function (user) {
                var formData = user;
                var defferer = $q.defer();

                $http.post('/admin/users', formData)
                    .success(function (data, status, headers, config) {
                        defferer.resolve(data);

                    })
                    .error(function (data, status, headers, config) {
                        var result = {
                            error : data,
                            status : status
                        };
                        defferer.reject(result);
                    });

                return defferer.promise;
            },

            deleteUser: function(id){
                var defferer = $q.defer();

                $http.delete("/admin/users/"+id)
                    .success(function (data){
                        return defferer.resolve(data);
                    })
                    .error(function (error){
                        return defferer.reject(error);
                    });
                return defferer.promise;
            }
        }
    }]);