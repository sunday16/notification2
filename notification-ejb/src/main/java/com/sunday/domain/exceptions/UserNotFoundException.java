package com.sunday.domain.exceptions;

/**
 * Created by michal on 29.11.15.
 */
public class UserNotFoundException extends DomainException {


    public UserNotFoundException(String message) {
        super(message);

    }
}


