package com.sunday.domain.exceptions;

import javax.ejb.ApplicationException;

/**
 * Created by michal on 29.11.15.
 */
@ApplicationException
public class DomainException extends Exception{
    public DomainException(String message) {
        super(message);
    }
}
