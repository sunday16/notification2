package com.sunday.domain.exceptions;

/**
 * Created by michal on 02.12.15.
 */
public class EventNotFoundException extends DomainException {
    public EventNotFoundException(String message) {
        super(message);
    }
}
