package com.sunday.domain.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by michal on 27.11.15.
 */
@Embeddable
public class Address implements Serializable{
    private String country;
    private String state;
    private String city;
    private String street;
    private String number;

    public Address() {
    }

    public Address(String country, String state, String city, String street, String number) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.street = street;
        this.number = number;
    }

    public static Address valueOf(String country, String state, String city, String street, String number) {
        return new Address(country, state, city, street, number);
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (!country.equals(address.country)) return false;
        if (!state.equals(address.state)) return false;
        if (!city.equals(address.city)) return false;
        if (!street.equals(address.street)) return false;
        return number.equals(address.number);

    }

    @Override
    public int hashCode() {
        int result = country.hashCode();
        result = 31 * result + state.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + street.hashCode();
        result = 31 * result + number.hashCode();
        return result;
    }
}
