package com.sunday.domain.model;

import com.sunday.domain.dto.EventDto;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.Date;

import static com.sunday.domain.model.Event.Query.*;
/**
 * Created by michal on 27.11.15.
 */

@Entity
@Table(name = "event")
@NamedQueries(
        {
                @NamedQuery(name = QUERY_GET_ALL_E, query = "SELECT n FROM com.sunday.domain.model.Event n"),
                @NamedQuery(name = QUERY_GET_COUNT_E, query = "SELECT count(n) FROM com.sunday.domain.model.Event n"),
                @NamedQuery(name = QUERY_GET_EVENT_BY_NAME, query = "SELECT n FROM com.sunday.domain.model.Event n WHERE n.name = :name")

        }
)
public class Event implements Serializable{

    public static final class Query {
        public static final String QUERY_GET_ALL_E = "getAllEvents";
        public static final String QUERY_GET_COUNT_E = "getCountEvents";
        public static final String QUERY_GET_EVENT_BY_NAME = "getEventByName";
    }

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Long id;


    @Column(name = "name")
    private String name;

    @Column(name = "short_descr")
    private String shortDescr;

    @Column(name = "long_descr")
    private String longDescr;

    @Column(name = "organizer")
    private String organizer;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_event")
    private Date dateEvent;

    @Embedded
    private Address address;

    @Embedded
    private LatLon latlon;

    public Event() {
    }

    public Event(String name, String shortDescr, String longDescr, String organizer, Date dateEvent, Address address, LatLon latlon) {
        this.name = name;
        this.shortDescr = shortDescr;
        this.longDescr = longDescr;
        this.organizer = organizer;
        this.dateEvent = dateEvent;
        this.address = address;
        this.latlon = latlon;
    }

    public Long getId() {
        return id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public void setShortDescr(String shortDescr) {
        this.shortDescr = shortDescr;
    }

    public String getLongDescr() {
        return longDescr;
    }

    public void setLongDescr(String longDescr) {
        this.longDescr = longDescr;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LatLon getLatlon() {
        return latlon;
    }

    public void setLatlon(LatLon latlon) {
        this.latlon = latlon;
    }

    public EventDto getEventInfo(){
        EventDto e = new EventDto(
                id,
                name,
                shortDescr,
                longDescr,
                organizer,
                dateEvent,
                address.getCountry(),
                address.getState(),
                address.getCity(),
                address.getStreet(),
                address.getNumber(),
                latlon.getLat(),
                latlon.getLon()
        );

        return e;
    }
}
