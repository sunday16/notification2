package com.sunday.domain.model;

import com.sunday.domain.dto.UserDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.sunday.domain.model.User.Query.*;

/**
 * Created by michal on 23.11.15.
 */
@Entity
@Table(name = "user")
@NamedQueries(
        {
                @NamedQuery(name = QUERY_GET_ALL, query = "SELECT n FROM com.sunday.domain.model.User n"),
                @NamedQuery(name = QUERY_COUNT, query = "SELECT count(n) FROM com.sunday.domain.model.User n"),
                @NamedQuery(name = QUERY_FIND_BY_EMAIL, query = "SELECT n FROM com.sunday.domain.model.User n WHERE n.email = :email"),
                @NamedQuery(name = QUERY_FIND_BY_EMAIL_FETCH_AUTHORITIES, query = "SELECT n FROM com.sunday.domain.model.User n left join FETCH n.authorities WHERE n.email = :email"),

                @NamedQuery(name = QUERY_DELETE_USER, query = "DELETE FROM com.sunday.domain.model.User n WHERE n.userId = :userId")
        }
)
public class User implements Serializable {

    public static final class Query {
        public static final String QUERY_GET_ALL = "getAllUser";
        public static final String QUERY_COUNT = "countAllUser";
        public static final String QUERY_FIND_BY_EMAIL = "findByEmail";
        public static final String QUERY_FIND_BY_EMAIL_FETCH_AUTHORITIES = "findByEmailFetchAuthorities";
        public static final String QUERY_DELETE_USER = "deleteUser";
    }

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @ManyToMany/*(cascade = {CascadeType.ALL})*/
    @JoinTable(
            name = "user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    private Set<Authority> authorities = new HashSet<>();

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public User() {
    }

    public User(String email, String firstName, String lastName, String password) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public UserDto getUserInfo() {
        UserDto u = new UserDto();
        u.setEmail(this.email);
        u.setFirstName(this.firstName);
        u.setLastName(this.lastName);
        u.setUserId(this.userId);

        return u;
    }

    public UserDto getUserInfoWithAuthorities() {
        UserDto u = getUserInfo();
        u.setAuthorities(this.authorities);
        return u;
    }

    public Long getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Authority> getAuthorities() {
        return Collections.unmodifiableSet(authorities);
    }

    public void  addAuthority(Authority authority){
        authorities.add(authority);
    }
}
