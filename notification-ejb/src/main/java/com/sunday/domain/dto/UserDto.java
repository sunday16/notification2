package com.sunday.domain.dto;

import com.sunday.domain.model.Authority;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by michal on 23.11.15.
 */
public class UserDto implements Serializable {
    private String email;


    private String firstName;
    private String lastName;
    private long userId;
    private String password;
    private Set<String> authorities;

    public UserDto() {
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userId=" + userId +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = new HashSet<>(authorities.size());

        for(Authority s : authorities)
        this.authorities.add(s.getName());
    }


}
