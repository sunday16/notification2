package com.sunday.domain.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by michal on 25.11.15.
 */
public class Paginator<T> implements Serializable{


    private static final long serialVersionUID = 164061116419328048L;
    public List<T> allResult;
    public long total;

    public Paginator(List<T> allResult, long total) {
        this.allResult = allResult;
        this.total = total;
    }

    public List<T> getAllResult() {
        return allResult;
    }

    public long getTotal() {
        return total;
    }
}
