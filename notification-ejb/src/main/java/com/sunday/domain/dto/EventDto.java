package com.sunday.domain.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by michal on 27.11.15.
 */
public class EventDto implements Serializable {
    private Long id;
    private String name;
    private String shortDescr;
    private String longDescr;
    private String organizer;
    private Date dateEvent;
    private String country;
    private String state;
    private String city;
    private String street;
    private String number;
    private double lat;
    private double lon;

    public EventDto() {
    }

    public EventDto(Long id, String name, String shortDescr, String longDescr, String organizer, Date dateEvent,
                    String country, String state, String city, String street, String number, double lat, double lon) {
        this.id = id;
        this.name = name;
        this.shortDescr = shortDescr;
        this.longDescr = longDescr;
        this.organizer = organizer;
        this.dateEvent = dateEvent;
        this.country = country;
        this.state = state;
        this.city = city;
        this.street = street;
        this.number = number;
        this.lat = lat;
        this.lon = lon;
    }

    public Long getId() {
        return id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public void setShortDescr(String shortDescr) {
        this.shortDescr = shortDescr;
    }

    public String getLongDescr() {
        return longDescr;
    }

    public void setLongDescr(String longDescr) {
        this.longDescr = longDescr;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "EventDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shortDescr='" + shortDescr + '\'' +
                ", longDescr='" + longDescr + '\'' +
                ", organizer='" + organizer + '\'' +
                ", dateEvent=" + dateEvent +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
