package com.sunday.domain.service;

import javax.ejb.Remote;

/**
 * Created by michal on 23.11.15.
 */
@Remote
public interface EventServiceRemote extends EventServiceLocal {
}
