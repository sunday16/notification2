package com.sunday.domain.service;

import com.sunday.domain.dto.Paginator;
import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.User;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by michal on 23.11.15.
 */
@Local
public interface UserServiceLocale  {
    UserDto createUser(User user);
    UserDto findUser(long userId);
    User findUserByEmail(String email) throws UserNotFoundException;
    Paginator<UserDto> getAllUser(int offset, int limit);
    void deleteUser(long userId);
    User findUserWithAuthorities(String email) throws UserNotFoundException;

    void changePassword(String password,User user) throws UserNotFoundException;
}

