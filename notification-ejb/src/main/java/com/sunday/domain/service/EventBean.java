package com.sunday.domain.service;

import com.sunday.domain.dto.EventDto;
import com.sunday.domain.dto.Paginator;
import com.sunday.domain.exceptions.EventNotFoundException;
import com.sunday.domain.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.RequestWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 27.11.15.
 */
@Stateless
public class EventBean implements EventServiceLocal, EventServiceRemote {

    public static final Logger log = LoggerFactory.getLogger(EventBean.class);

    @PersistenceContext
    EntityManager em;


    @Override
    public EventDto createEvent(Event event) {
        em.persist(event);
        return event.getEventInfo();
    }

    public EventDto findEvent(long id) {
        log.info("Find event");
        Event event = em.find(Event.class, id);
        return event.getEventInfo();
    }

    public EventDto findEventByName(String name) throws EventNotFoundException {
        List<Event> event = em.createNamedQuery(Event.Query.QUERY_GET_EVENT_BY_NAME, Event.class)
                .setParameter("name", name)
                .getResultList();
        return event.get(0).getEventInfo();
    }

    @Override
    public EventDto updateEvent(long id, Event event) {
        Event eventUpdate = em.find(Event.class, id);
        eventUpdate.setName(event.getName());
        eventUpdate.setShortDescr(event.getShortDescr());
        eventUpdate.setLongDescr(event.getLongDescr());
        eventUpdate.setOrganizer(event.getOrganizer());
        eventUpdate.setDateEvent(event.getDateEvent());
        eventUpdate.setAddress(event.getAddress());
        eventUpdate.setLatlon(event.getLatlon());
        em.merge(eventUpdate);
        return eventUpdate.getEventInfo();
    }

    @Override
    public Paginator<EventDto> getAllEvent(int offset, int limit) {
        log.info("\nGet All Events");
        long total = em.createNamedQuery(Event.Query.QUERY_GET_COUNT_E, Long.class)
                .getSingleResult();
        List<Event> query = em.createNamedQuery(Event.Query.QUERY_GET_ALL_E, Event.class)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
        List<EventDto> result = new ArrayList<>(query.size());
        for (Event e : query) {
            result.add(e.getEventInfo());
        }

        return new Paginator<>(result, total);
    }

    @Override
    public void deleteEvent(long id){
        Event event = em.find(Event.class, id);
        em.remove(event);
    }


}
