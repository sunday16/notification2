package com.sunday.domain.service;

import com.sunday.domain.dto.EventDto;
import com.sunday.domain.dto.Paginator;
import com.sunday.domain.exceptions.EventNotFoundException;
import com.sunday.domain.model.Event;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by michal on 27.11.15.
 */
@Local
public interface EventServiceLocal {

    EventDto createEvent(Event event);
    EventDto findEvent(long id);
    Paginator<EventDto> getAllEvent(int offset, int limit);
    EventDto findEventByName(String name) throws EventNotFoundException;
    EventDto updateEvent(long id,Event event);
    void deleteEvent(long id);
}
