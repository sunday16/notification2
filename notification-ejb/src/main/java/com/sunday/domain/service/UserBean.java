package com.sunday.domain.service;

import com.sunday.domain.dto.Paginator;
import com.sunday.domain.dto.UserDto;
import com.sunday.domain.exceptions.UserNotFoundException;
import com.sunday.domain.model.Authority;
import com.sunday.domain.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal on 23.11.15.
 */
@Stateless
public class UserBean implements UserServiceLocale, UserServiceRemote {

    private static final Logger log = LoggerFactory.getLogger(UserBean.class);

    @PersistenceContext
    EntityManager em;


    @Override
    public UserDto createUser(User user) {
        Authority authority = new Authority();
        authority.setName("ROLE_USER");
        user.addAuthority(authority);
        em.persist(user);
        return user.getUserInfo();
    }


    @Override
    public UserDto findUser(long userId) {
        User user = em.find(User.class, userId);
        return user.getUserInfo();
    }

    @Override
    public User findUserWithAuthorities(String email) throws UserNotFoundException {
        List<User> users = em.createNamedQuery(User.Query.QUERY_FIND_BY_EMAIL_FETCH_AUTHORITIES, User.class)
                .setParameter("email", email)
                .getResultList();

        if (!users.isEmpty()) {
            return users.get(0);
        }

        throw new UserNotFoundException(email);

    }

    public Paginator<UserDto> getAllUser(int offset, int limit) {
        long total = em.createNamedQuery(User.Query.QUERY_COUNT, Long.class).getSingleResult();
        List<User> query = em.createNamedQuery(User.Query.QUERY_GET_ALL, User.class)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();

        List<UserDto> allResult = new ArrayList<>(query.size());
        for (User u : query) {
            allResult.add(u.getUserInfo());
        }

        Paginator<UserDto> pageResult = new Paginator<>(allResult, total);

        return pageResult;
    }

    @Override
    public void deleteUser(long userId) {
        User user = em.find(User.class, userId);
        em.remove(user);
    }

    public User findUserByEmail(String email) throws UserNotFoundException {
        List<User> users = em.createNamedQuery(User.Query.QUERY_FIND_BY_EMAIL, User.class)
                .setParameter("email", email)
                .getResultList();
        if (users.isEmpty()) {
            throw new UserNotFoundException(email);
        }

        return users.get(0);
    }

    @Override
    public void changePassword(String password, User user) throws UserNotFoundException {
        em.find(User.class, user.getUserId());
        user.setPassword(password);
        em.merge(user);
    }
}
